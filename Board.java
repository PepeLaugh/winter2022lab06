public class Board
{
    private Die firstDie;
    private Die secondDie;
    private boolean[] closedTiles;

    public Board()
    {
        this.firstDie = new Die();
        this.secondDie = new Die();
        this.closedTiles = new boolean[12];
    }
    public String toString()
    {
        String check = " ";
        for(int i = 0; i < closedTiles.length; i++)
        {
            if(closedTiles[i] == false)
            {
                check += (i+1) + " ";
            }
            else
            {
                check += "X ";
            }
        }
        return check;
    }
    public boolean playATurn()
    {
        firstDie.roll();
        secondDie.roll();
        
        System.out.println(firstDie.toString());
        System.out.println(secondDie.toString());

        int sum = firstDie.getPips() + secondDie.getPips();
        boolean isClosed = false;
        if(closedTiles[sum-1] == false)
        {
            closedTiles[sum-1] = true;
            System.out.println("closing tile : " + sum);
        }
        else
        {
            System.out.println("tile already shut");
            isClosed = true;
        }
        return isClosed;
    }
}