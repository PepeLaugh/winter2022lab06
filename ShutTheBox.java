public class ShutTheBox
{
    public static void main(String[]args)
    {
      System.out.println("Welcome to the best ShutTheBox game ever");  

      Board boardGame = new Board();
      boolean gameOver = false;
      
      while(gameOver == false)
      {
        System.out.println("Player 1's Turn");
        System.out.println(boardGame.toString());
     
        if(boardGame.playATurn())
        {
            System.out.println("Player 2 has Won the Game");
            gameOver = true;
        }
        else
        {
            System.out.println("Player 2's Turn");
            System.out.println(boardGame.toString());

            if(boardGame.playATurn())
            {
                System.out.println("Player 1 has Won the Game");
                gameOver = true;
            }
        } 
      }
    }
}